"""
The values module handles the rendering an presentation of non literal data
types (like `None`). This is used to create a more meaningful report.
"""

from datetime import date
from typing import Optional, Union

Value = Union[bool, None, str, date]
"""All data types which are supported by the `ValueDisplay` class."""


class ValueDisplay:
    """
    Creates a meaningful representation of non literal (e.g. non string)
    values.

    Remark: As this class is used to create reports the `None` value is
    generally considered a bad thing; it will return a warning.
    """

    value: Value
    date_format: Optional[str] = None
    """
    Format used to render dates. It defaults (property set to `None`) to the
    ISO 8601 format (`YYYY-MM-DD`) will be used.
    """

    def __init__(
        self,
        value: Value,
        date_format: Optional[str] = None
    ) -> None:
        if not isinstance(value, Value.__args__):
            raise TypeError
        self.value = value
        self.date_format = date_format

    def __str__(self) -> str:
        if self.value is None:
            return self.__none()
        if isinstance(self.value, str):
            return self.value
        if isinstance(self.value, bool):
            return self.__bool(self.value)
        if isinstance(self.value, date):
            return self.__date(self.value)

    @staticmethod
    def __none() -> str:
        return "Unknown (!)"

    @staticmethod
    def __bool(value: bool) -> str:
        if value:
            return "Yes ✓"
        return "No ✗"

    def __date(self, value: date) -> str:
        if self.date_format is None:
            return value.isoformat()
        return date.strftime(self.date_format)
