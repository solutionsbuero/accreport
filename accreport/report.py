"""
The report module contains the general logic for the PDF generation.
"""

from abc import ABCMeta, abstractmethod
from datetime import date, datetime
from pathlib import Path
from typing import Generic, List, Optional, Tuple, TypeVar, Union

from accthymemodels.models.invoice import Invoice
from accthymemodels.models.enums import PaymentMode
from accthymemodels.models.employees import Employee
from accthymemodels.models.expense import Expense
from accthymemodels.models.miscrecord import MiscRecord
from accthymemodels.models.organization import Organization

from reportlab.graphics.barcode.dmtx import DataMatrixWidget
from reportlab.graphics.shapes import Drawing
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm
from reportlab.platypus import (
    Flowable,
    PageBreak,
    Paragraph,
    SimpleDocTemplate,
    Table,
    TableStyle
)
from reportlab.lib.styles import ParagraphStyle

from accreport.attachment import AttachmentFactory
from accreport.exception import FileTypeError
from accreport.values import Value, ValueDisplay


T = TypeVar("T")


class Report(Generic[T], metaclass=ABCMeta):
    """
    The base for all different kinds of report. The reports should be
    initialized using the `ReportFactory`. This class handles most of the
    PDF formatting.
    """

    PAGESIZE = A4
    PAGE_MARGIN = 8 * mm

    record: T
    company: Organization

    def __init__(self, record: T, company: Organization) -> None:
        self.record = record
        self.company = company

    @staticmethod
    def type_title() -> str:
        """Returns the name of the Type used in the report title."""
        pass

    @property
    @abstractmethod
    def unique(self) -> str:
        """Returns the unique id (UUID) of the record as a string."""
        pass

    @property
    @abstractmethod
    def ident(self) -> str:
        """Returns the human readable identifier of the record."""
        pass

    @property
    @abstractmethod
    def name(self) -> str:
        """Returns the name of the record as a string."""
        pass

    @property
    @abstractmethod
    def data(self) -> List[Tuple[str, Value]]:
        """
        Returns a list of tuples containing additional information about the
        record. The tuples should follow the (Key/Description, Value) format.
        This will create the resulting header in the PDF export.
        """
        pass

    @property
    @abstractmethod
    def attachment(self) -> Optional[Path]:
        """
        Path the record/recipe for this data entry. Should be an PDF or image
        file.
        """
        pass

    def generate(self, path: Path):
        """Builds the PDF and saves it to the given path."""
        doc = self.__build_document(path)
        elements = [
            self.__build_header(),
            self.__build_data(),
        ]
        elements += self.__build_content(doc.width)

        doc.build(elements)

    def __build_document(self, path: Path) -> SimpleDocTemplate:
        rsl = SimpleDocTemplate(
            path,
            pagesize=self.PAGESIZE,
            leftMargin=self.PAGE_MARGIN,
            rightMargin=self.PAGE_MARGIN,
            topMargin=self.PAGE_MARGIN,
            bottomMargin=self.PAGE_MARGIN,
        )
        rsl.title = f"{self.type_title()} {self.ident} − {self.name}"
        rsl.subject = f"{self.type_title()} Report"
        rsl.author = self.company.name
        rsl.keywords = f"acc, {self.type_title()}, report"
        return rsl

    def __build_header(self) -> Table:
        data = [
            [f"{self.type_title()} {self.ident}", self.unique],
            [self.name, ""],
        ]
        style = TableStyle([
            ("BOX", (0, 0), (1, 1), 0.8, colors.black),
            ("FONTSIZE", (0, 0), (1, 1), 9),
            ("LINEBELOW", (0, 0), (1, 0), 0.1, colors.black),
            ("FONT", (0, 0), (0, 0), "Helvetica-Bold", 20),
            ("FONT", (1, 0), (1, 0), "Courier"),
            ("ALIGN", (1, 0), (1, 0), "RIGHT"),
            ("VALIGN", (1, 0), (1, 0), "BOTTOM"),
        ])
        table = Table(data, colWidths="*")
        table.setStyle(style)
        return table

    def __build_data(self) -> Table:
        try:
            tuples = self.data
        except AttributeError as e:
            raise AttributeError(f"On exporting '{self.attachment}' {e}")


        # Convert all values to strings.
        str_tuples: List[Tuple[str, str]] = []
        for pair in tuples:
            str_tuples.append((
                pair[0],
                str(ValueDisplay(pair[1]))
            ))

        # Split data into sublists of two tuples (one line in header).
        lines = [str_tuples[i:i+2] for i in range(0, len(str_tuples), 2)]

        # Two Key-Value-Pairs per line.
        data: List[List[str]] = []
        for line in lines:
            if len(line) == 1:
                data.append([line[0][0], line[0][1]])
                continue
            data.append([line[0][0], line[0][1], line[1][0], line[1][1]])
        max_data_col = len(data[0]) - 1
        max_data_row = len(data) - 1

        style = TableStyle([
            ("BOX", (0, 0), (max_data_col, max_data_row), 0.8, colors.black),
            ("TOPPADDING", (0, 0), (max_data_col, max_data_row), 2),
            ("BOTTOMPADDING", (0, 0), (max_data_col, max_data_row), 2),
            (
                "LINEABOVE",
                (0, 0),
                (max_data_col, max_data_row),
                0.1, colors.black,
            ),
            (
                "LINEBELOW",
                (0, max_data_row),
                (max_data_col, max_data_row),
                0.1,
                colors.black,
            ),
            ("LINEBEFORE", (0, 0), (0, max_data_row), 0.1, colors.black),
            ("LINEAFTER", (1, 0), (1, max_data_row), 0.1, colors.black),
            (
                "LINEAFTER",
                (max_data_col, 0),
                (max_data_col, max_data_row),
                0.1,
                colors.black
            ),
            ("FONTSIZE", (0, 0), (max_data_col, max_data_row), 8),
            ("ALIGN", (0, 0), (0, max_data_row), "RIGHT"),
            ("TEXTCOLOR", (0, 0), (0, max_data_row), colors.dimgray),
            ("RIGHTPADDING", (0, 0), (0, max_data_row), 0),
            ("ALIGN", (2, 0), (2, max_data_row), "RIGHT"),
            ("TEXTCOLOR", (2, 0), (2, max_data_row), colors.dimgray),
            ("RIGHTPADDING", (2, 0), (2, max_data_row), 0),
        ])

        table = Table(data, colWidths=[23 * mm, "*", 23 * mm, "*"])
        table.setStyle(style)
        return table

    def __build_content(
        self,
        doc_width: float,
    ) -> List[Union[Table, Flowable]]:
        rsl = []
        try:
            attachment = AttachmentFactory.from_path(self.attachment)
        except FileTypeError as e:
            raise FileTypeError(f"{self.ident}: {e}")
        pages = attachment.get_pages(self.attachment)
        for i in range(1, pages+1):
            if i != 1:
                rsl.append(PageBreak())
                rsl.append(self.__build_header())
                max_height = 245 * mm
            else:
                max_height = 218 * mm
            page = attachment(
                Path(self.attachment), i, doc_width - 8 * mm, max_height
            )
            data = [[page]]
            table = Table(data, colWidths="*")
            style = TableStyle([
                ("BOX", (0, 0), (0, 0), 0.8, colors.black),
                ("ALIGN", (0, 0), (0, 0), "CENTER"),
                ("LEFTPADDING", (0, 0), (0, 0), 1),
                ("TOPPADDING", (0, 0), (0, 0), 1),
                ("RIGHTPADDING", (0, 0), (0, 0), 1),
                ("BOTTOMPADDING", (0, 0), (0, 0), 1),
            ])
            table.setStyle(style)
            rsl.append(table)
            rsl.append(self.__build_footer(i, pages))
        return rsl

    def __build_footer(self, current_page: int, max_page: int) -> Table:
        company = Paragraph(
            f"{self.company.name}<br/>"
            f"{self.company.street}<br/>"
            f"{self.company.postalCode} {self.company.place}",
            ParagraphStyle("small-left", fontSize=8, leading=8)
        )
        info = Paragraph(
            f"{current_page} / {max_page}<br/>"
            f"Report created on {datetime.now().isoformat()}",
            ParagraphStyle("small-left", fontSize=8, leading=8, alignment=1)
        )

        bc_widget = DataMatrixWidget(value=str(self.record.unique), x=0, y=0)
        bounds = bc_widget.getBounds()
        width = bounds[3] - bounds[0]
        height = bounds[2] - bounds[1]
        barcode = Drawing(mm, mm, transform=[7.8 * mm/width, 0, 0, 7.8 * mm/height, -6.4 * mm, -7.7 * mm])
        barcode.add(bc_widget)

        data = [[company, info, barcode]]
        style = TableStyle([
            ("BOX", (0, 0), (2, 0), 0.8, colors.black),
            ("VALIGN", (0, 0), (2, 0), "TOP"),
            ("TOPPADDING", (0, 0), (2, 0), 2),
            ("ALIGN", (2, 0), (2, 0), "RIGHT"),
        ])

        table = Table(data, colWidths="*")
        table.setStyle(style)
        return table


class ExpenseReport(Report[Expense]):
    """Report for an Acc Expense."""

    @staticmethod
    def type_title() -> str:
        return "Expense"

    @property
    def unique(self) -> str:
        return str(self.record.unique)

    @property
    def ident(self) -> str:
        return self.record.short

    @property
    def name(self) -> str:
        return self.record.name

    @property
    def data(self) -> List[Tuple[str, Value]]:
        if self.record.advancedBy is not None and \
                not isinstance(self.record.advancedBy, Employee):
            raise TypeError(
                "Got UUID for advancing employee instead of Employee instance"
            )
        if self.record.advancedBy is not None:
            advanced_by = f"{self.record.advancedBy.name} (y-??)"
        else:
            advanced_by = "Not advanced"

        if self.record.dateOfSettlement is None:
            settlement_date = "Not settled yet"
        else:
            settlement_date = self.record.dateOfSettlement

        if self.record.settlementTransactionId is None:
            if self.record.paymentMode == PaymentMode.BANKTRANSFER:
                settlement_transaction = "-"
            else:
                settlement_transaction = "-"
        else:
            settlement_transaction = self.record.settlementTransactionId

        return [
            (
                "Project",
                f"{self.record.project.name} ({self.record.project.short})"
            ),
            (
                "Customer",
                f"{self.record.project.customer.name} ({self.record.project.customer.short})"
            ),
            ("Amount", str(self.record.figure)),
            ("Exp. Category", self.record.expenseCategory),
            ("Internal", self.record.internal),
            ("Billable", self.record.billable),
            ("P. Method", self.record.paymentMode.name.capitalize()),
            ("Advanced by", advanced_by),
            ("Accrued on", self.record.dateOfAccrual),
            ("Settled on", settlement_date),
        ]

    @property
    def attachment(self) -> Optional[Path]:
        return self.record.path


class InvoiceReport(Report[Invoice]):
    """Report for an Acc Invoice:"""

    @staticmethod
    def type_title() -> str:
        return "Invoice"

    @property
    def unique(self) -> str:
        return str(self.record.unique)

    @property
    def ident(self) -> str:
        return self.record.short

    @property
    def name(self) -> str:
        return self.record.name

    @property
    def data(self) -> List[Tuple[str, Value]]:
        if self.record.dateOfSettlement is None:
            settlement_date = "Not settled yet"
        else:
            settlement_date = self.record.dateOfSettlement

        if self.record.settlementTransactionId is None:
            settlement_transaction = "-"
        else:
            settlement_transaction = self.record.settlementTransactionId

        return [
            (
                "Project",
                f"{self.record.project.name} ({self.record.project.short})"
            ),
            (
                "Customer",
                f"{self.record.project.customer.name} ({self.record.project.customer.short})"
            ),
            ("Amount", str(self.record.figure)),
            ("Send on", self.record.sendDate),
            ("Settled on", settlement_date),
            ("Settl. Tr.", settlement_transaction),
        ]

    @property
    def attachment(self) -> Optional[Path]:
        return self.record.path


class MiscReport(Report[MiscRecord]):
    """Report for Acc MiscRecord."""

    @staticmethod
    def type_title() -> str:
        return "Misc Record"

    @property
    def unique(self) -> str:
        return str(self.record.unique)

    @property
    def ident(self) -> str:
        return self.record.short

    @property
    def name(self) -> str:
        return self.record.name

    @property
    def data(self) -> List[Tuple[str, Value]]:
        return [
            (
                "Project",
                f"{self.record.project.name} ({self.record.project.short})"
            ),
            (
                "Customer",
                f"{self.record.project.customer.name} ({self.record.project.customer.short})"
            ),
            ("Date of accural", self.record.dateOfAccrual),
        ]

    @property
    def attachment(self) -> Optional[Path]:
        return self.record.path


class ReportFactory:
    """
    Creates the correct `Report` implementation based on the given record type.
    """

    @classmethod
    def from_record(cls, record: T, company: Organization) -> Report:
        """
        Creates and returns the correct `Record` implementation for the
        given record.
        """

        if isinstance(record, Expense):
            return ExpenseReport(record, company)
        if isinstance(record, Invoice):
            return InvoiceReport(record, company)
        if isinstance(record, MiscRecord):
            return MiscReport(record, company)
        raise NotImplementedError(
            f"Report is not implemented for {type(record)}"
        )
