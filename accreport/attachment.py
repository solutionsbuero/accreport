"""
The attachment module handles the embedding of PDF/image documents into the
report.
"""
from abc import ABCMeta, abstractmethod, abstractclassmethod
import mimetypes
from pathlib import Path
from typing import Any, List

from pdfrw import PdfReader
from pdfrw.buildxobj import pagexobj
from pdfrw.toreportlab import makerl
from PIL import Image as PilImage
from reportlab.lib.utils import ImageReader
from reportlab.platypus import Flowable

from accreport.exception import FileTypeError


class Attachment(Flowable, metaclass=ABCMeta):
    """
    Flowable to embed an Attachment within an Reportlab PDF document. Use the
    `AttachmentFactory` to get the correct implementation (depending whether
    the file is a PDF or a image).
    """

    def __init__(self, path: Path, page: int) -> None:
        pass

    @abstractclassmethod
    def get_pages(cls, path: Path) -> int:
        """Returns the number of pages of a given attachment."""
        pass

    @abstractclassmethod
    def allowed_types(cls) -> List[str]:
        """
        Returns a list of allowed MIME-types for a given attachment
        implementation.
        """
        pass

    @staticmethod
    def _mimetype(path: Path) -> str:
        return mimetypes.guess_type(str(path))[0]

    @classmethod
    def __valid_file(cls, path: Path) -> bool:
        """
        Returns whether the type of the file with the given path can be
        embedded in the PDF report (image/PDF).
        """
        return Attachment.__mimetype(path) in cls.allowed_types()

    @abstractmethod
    def wrap(self, available_width, available_height):
        """Returns the available width and height."""
        pass

    @abstractclassmethod
    def drawOn(self, canvas, x, y, _sW=0):
        """Draws to the canvas."""
        pass


class PdfAttachment(Attachment):
    """Implements the `Attachment` class for the PDF file-type."""

    page: int
    """Page number."""
    draw_width: float
    draw_height: float
    xobj: Any
    _w: float
    _h: float

    def __init__(
        self,
        path: Path,
        page: int,
        max_width: float,
        max_height: float,
    ) -> None:
        pdf = PdfReader(path, decompress=False).pages[page-1]
        self.xobj = pagexobj(pdf)

        x1, y1, x2, y2 = self.xobj.BBox
        self._w, self._h = x2 - x1, y2 - y1

        factor = min(float(max_width)/self._w, float(max_height) / self._h)
        self.draw_width = self._w*factor
        self.draw_height = self._h*factor

    @classmethod
    def get_pages(cls, path: Path) -> int:
        return len(PdfReader(path, decompress=False).pages)

    @classmethod
    def allowed_types(cls) -> List[str]:
        return ["application/pdf"]

    def wrap(self, available_width, available_height):
        return self.draw_width, self.draw_height

    def drawOn(self, canvas, x, y, _sW=0):
        xobj_name = makerl(canvas, self.xobj)

        x_scale = self.draw_width/self._w
        y_scale = self.draw_height/self._h

        x -= self.xobj.BBox[0] * x_scale
        y -= self.xobj.BBox[1] * y_scale

        canvas.saveState()
        canvas.translate(x, y)
        canvas.scale(x_scale, y_scale)
        canvas.doForm(xobj_name)
        canvas.restoreState()


class ImageAttachment(Attachment):
    """Implements the `Attachment` class for images."""

    image: PilImage
    draw_width: float
    draw_height: float

    def __init__(
        self,
        path: Path,
        page: int,
        max_width: float,
        max_height: float,
    ) -> None:
        self.image = PilImage.open(path)
        width, height = self.image.size

        factor = min(float(max_width)/width, float(max_height) / height)
        self.draw_width = width * factor
        self.draw_height = height * factor

    @classmethod
    def get_pages(cls, path: Path) -> int:
        return 1

    @classmethod
    def allowed_types(cls) -> List[str]:
        return [
            "image/gif",
            "image/jpeg",
            "image/png",
        ]

    def wrap(self, available_width, available_height):
        return self.draw_width, self.draw_height

    def drawOn(self, canvas, x, y, _sW=0):
        canvas.drawImage(
            ImageReader(self.image),
            x,
            y,
            self.draw_width,
            self.draw_height,
            "auto"
        )


class AttachmentFactory:
    """Factory for `Attachment`."""

    @classmethod
    def from_path(cls, path: Path) -> Attachment:
        """
        Returns the correct `Attachment` implementation based on the file
        with the given path.
        """
        mtype = Attachment._mimetype(path)
        if mtype in PdfAttachment.allowed_types():
            return PdfAttachment
        if mtype in ImageAttachment.allowed_types():
            return ImageAttachment
        raise FileTypeError(f"asset '{path}' is not a PDF or image")
