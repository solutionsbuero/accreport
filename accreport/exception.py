"""
Custom exceptions for accreport.
"""


class FileTypeError(Exception):
    """Exception when a file has an invalid type."""
    pass
