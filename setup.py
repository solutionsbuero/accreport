import setuptools

setuptools.setup(
    name="accreport",
    version="0.1.0",
    author="Genossenschaft Solutionsbüro",
    author_email="msg@frg72.com",
    packages=setuptools.find_packages(),
    include_package_data=True,
    install_requires=[
        'gitdb',
        'GitPython',
        'pdfrw',
        'peewee',
        'Pillow',
        'pydantic',
        'pyYAML',
        'reportlab',
        'smmap',
        'typing-extensions',
        'accthymemodels @ git+https://gitlab.com/solutionsbuero/accthymemodels.git'
    ]
)
