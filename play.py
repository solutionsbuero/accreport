from pathlib import Path
from datetime import date

from accthymemodels.accstrategy import AccStrategy
from accthymemodels.mediator import DataMediator
from accthymemodels.models.accthymeconfig import AccConfig, AccThymeConfig
from accthymemodels.models.customer import CustomerCreation
from accthymemodels.models.employees import EmployeeCreation
from accthymemodels.models.expense import ExpenseCreation
from accthymemodels.models.invoice import InvoiceCreation
from accthymemodels.models.organization import Organization
from accthymemodels.models.project import ProjectCreation

from accreport.report import ReportFactory


if __name__ == "__main__":
    config = AccThymeConfig(acc=AccConfig(basepath=Path("/tmp/hoi")))
    mediator = DataMediator(config)
    strategy = AccStrategy(mediator)
    mediator.register_accfactory(strategy)

    customer = CustomerCreation(
        name="Dumm",
    )
    customer = strategy.save_customer(customer)
    employee = EmployeeCreation(
        name="Max Musterarbeiter",
        email="max@fantasia.army",
    )
    employee = strategy.save_employee(employee)
    project = ProjectCreation(
        name="Stupid Project",
        customer=customer.unique,
    )
    project = strategy.save_project(project)
    input("copy asset")

    exp = ExpenseCreation(
        name="Werbeunterdeckelshit für die Wohnung",
        project=project.unique,
        figure="EUR 15,00",
        advancedBy=employee.unique,
        path="/tmp/hoi/projects/dumm/stupid-project/printer.pdf",
    )
    # exp = strategy.save_asset(exp)

    inv = InvoiceCreation(
        name="Rechnung Plattform",
        project=project.unique,
        figure="EUR 210,00",
        sendDate=date(2021, 2, 7),
        path="/tmp/hoi/projects/dumm/stupid-project/printer.pdf",
    )
    inv = strategy.save_asset(inv)

    organization = Organization(
        name="Fantasia Inc.",
        street="Südhauser Straße 4",
        place="Zürich",
        postalCode="8000",
        email="info@fantasia.army",
    )
    report = ReportFactory.from_record(inv, organization)
    report.generate("test/hoi.pdf")

